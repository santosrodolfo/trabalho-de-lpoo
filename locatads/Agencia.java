package locatads;

import java.util.ArrayList;
import java.util.Scanner;

public class Agencia {
	public int codigoAgencia;
	public String cidade, uf;
	public ArrayList<Carro> lista_de_carros = new ArrayList<Carro>();


	public Agencia(String cidade, String uf, int codigoAgencia) {
		this.cidade = cidade;
		this.codigoAgencia = codigoAgencia;
		this.uf = uf;
	}

	public Agencia() {}

	public void print() {
		System.out.println("");
		System.out.println("  Código Agência: " + this.codigoAgencia);
		System.out.println("  Cidade: " + this.cidade + " - " + this.uf);
	}

	public void scan() {
		Scanner s = new Scanner(System.in);
		System.out.println("  não é permitido caracters ex:(. , ; * - _) ");
		System.out.print("  Codigo Agência: ");
		this.codigoAgencia = s.nextInt();

		s.nextLine();
		System.out.print("  Cidade: ");
		this.cidade = s.nextLine();

		System.out.print("  UF: ");
		this.uf = s.nextLine();

	}

	public void cadastraCarro(){
		Carro carro = new Carro();
		carro.scan();
		this.lista_de_carros.add(carro);
	}

	public void imprimeCarro(){
		for(int i = 0; i < lista_de_carros.size(); i++) {
			Carro temp = this.lista_de_carros.get(i);
			temp.print();
		}
	}
}
