package locatads;

import java.util.ArrayList;
import java.util.Scanner;

public class Cliente {
    public String nome, sobrenome;
    public String cpf, cnh;
    public int mes, dia, ano;

    public Cliente(String nome, String sobrenome, String cpf, String cnh, int mes, int dia, int ano) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.cpf = cpf;
        this.cnh = cnh;
        this.mes = mes;
        this.dia = dia;
        this.ano = ano;
    }

    public Cliente() {}

    public void print() {
        System.out.println("");
        System.out.println("  Nome: " + this.nome + " " + this.sobrenome);
        System.out.println("  Data nasc: " + this.dia + "/" + this.mes + "/" + this.ano);
        System.out.println("  Cpf: " + this.cpf + " Cnh: " + this.cnh);
        System.out.println("");
    }

    public void scan() {
        Scanner s = new Scanner(System.in);
        System.out.print("  Nome: ");
        this.nome = s.nextLine();
        System.out.print("  Sobrenome: ");
        this.sobrenome = s.nextLine();

        System.out.println("  não é permitido caracters ex:(. , ; * - _) ");
        System.out.print("  Cpf: ");
        this.cpf = s.nextLine();
        System.out.print("  Cnh: ");
        this.cnh = s.nextLine();

        System.out.println("  Informe a data de nascimento. ");
        System.out.print("  Dia: ");
        this.dia = s.nextInt();
        System.out.print("  Mês: ");
        this.mes = s.nextInt();
        System.out.print("  Ano: ");
        this.ano = s.nextInt();
    }
}