package locatads;

import java.util.ArrayList;
import java.util.Scanner;

public class Aluguel {
    public String placa, senhaFunc;
    public String cnhCliente;
    public int codagencia;

    public Aluguel(String placa, String senhaFunc, String cnhCliente, int codagencia) {
        this.senhaFunc = senhaFunc;
        this.cnhCliente = cnhCliente;
        this.placa = placa;
        this.codagencia = codagencia;
    }

    public Aluguel() {}

    public void scan() {
        Scanner s = new Scanner(System.in);

        System.out.print("  Senha funcionário: ");
        this.senhaFunc = s.nextLine();

        System.out.print("  Informe a cnh do cliente: ");
        this.cnhCliente = s.nextLine();

        //System.out.print("  Informe o código da agência: ");
        //this.codagencia = s.nextLine();

        System.out.print("  Informe a placa do carro que deseja alugar: ");
        this.placa = s.nextLine();
    }
}