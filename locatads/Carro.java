package locatads;

import java.util.ArrayList;
import java.util.Scanner;

public class Carro {
    public String placa, nome, marca;
    public int ano, modelo, codigoAgencia;
    public double valor;
    public boolean alugado;

    public Carro(String nome, String marca, int ano, int modelo, String placa, 
        double valor) {

        this.nome = nome;
        this.marca = marca;
        this.ano = ano;
        this.modelo = modelo;
        this.placa = placa;
        this.valor = valor;
        this.alugado = false;
    }

    public Carro() {}

    public void print() {
        System.out.println("");
        System.out.println("  Carro: " + this.nome + " Marca: " + this.marca);
        System.out.println("  Ano: " + this.ano + " Modelo: " + this.modelo);
        System.out.println("  Placa do carro: " + this.placa);
        System.out.println("  Valor do Aluguel: " + this.valor);
        System.out.println("  Aluguel: " + this.alugado);
        System.out.println("");
    }

    public void scan() {
        Scanner s = new Scanner(System.in);
        System.out.print("  Informe o nome do carro: ");
        this.nome = s.nextLine();
        System.out.print("  Informe a marca do carro: ");
        this.marca = s.nextLine();
        System.out.print("  Informe a placa do carro: ");
        this.placa = s.nextLine();
        System.out.print("  Informe o ano do carro: ");
        this.ano = s.nextInt();
        System.out.print("  Informe o modelo do carro: ");
        this.modelo = s.nextInt();
        System.out.print("  Informe o valor do aluguel: ");
        this.valor = s.nextDouble();
    }
}


