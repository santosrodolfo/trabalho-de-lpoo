package locatads;

import java.util.ArrayList;
import java.util.Scanner;

public class Funcionario {
    public String nome, sobrenome;
    public String cpf, senha;
    public int mes, dia, ano;

    public Funcionario(String nome, String sobrenome, String cpf, int mes, int dia, int ano, String senha) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.senha = senha;
        this.cpf = cpf;
        this.mes = mes;
        this.dia = dia;
        this.ano = ano;
    }

    public Funcionario() {}

    public void print() {
        System.out.println("");
        System.out.println("  Nome: " + this.nome + " " + this.sobrenome);
        System.out.println("  Data nasc: " + this.dia + "/" + this.mes + "/" + this.ano);
        System.out.println("  Cpf: " + this.cpf);
        System.out.println("");
    }

    public void scan() {
        Scanner s = new Scanner(System.in);
        System.out.print("  Nome: ");
        this.nome = s.nextLine();
        System.out.print("  Sobrenome: ");
        this.sobrenome = s.nextLine();
        System.out.print("  Senha: ");
        this.senha = s.nextLine();

        System.out.println("  não é permitido caracters ex:(. , ; * - _) ");
        System.out.print("  Cpf: ");
        this.cpf = s.nextLine();

        System.out.println("  Informe a data de nascimento. ");
        System.out.print("  Dia: ");
        this.dia = s.nextInt();
        System.out.print("  Mês: ");
        this.mes = s.nextInt();
        System.out.print("  Ano: ");
        this.ano = s.nextInt();
    }
}