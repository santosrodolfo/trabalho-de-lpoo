import locatads.*;

import java.util.ArrayList;
import java.util.Scanner;

public class MainLocatads {
	public static void main(String[] args) {


		int opcao;
		Scanner op = new Scanner(System.in);

		ArrayList<Agencia> lista_de_agencias = new ArrayList<Agencia>();
		ArrayList<Funcionario> lista_de_funcionarios = new ArrayList<Funcionario>();
		ArrayList<Cliente> lista_de_clientes = new ArrayList<Cliente>();


		do {

			System.out.println("---------------Escolha uma Opção---------------");
			System.out.println("      1 - Cadastrar Agência.");
			System.out.println("      2 - Cadastrar Funcionario.");
			System.out.println("      3 - Cadastrar Carro.");
			System.out.println("      4 - Cadastrar Cliente.");
			System.out.println("      5 - Aluguel.");
			System.out.println("      6 - Entrega.");
			System.out.println("      0 - Encerrar.");
			System.out.println("-----------------------------------------------");
			System.out.print("  Qual operação deseja realizar: ");
			opcao = op.nextInt();

			System.out.println("");

			switch (opcao) {
				case 1:
					System.out.println("-----Opção 1 selecionada.-----");
					System.out.println(" Cadastrar Agência: ");
					System.out.println("");

					Agencia agencia = new Agencia();
					agencia.scan();
					lista_de_agencias.add(agencia);

					System.out.println("");
					System.out.println(" Agências cadastradas:");
					for(int i = 0; i < lista_de_agencias.size(); i++) {
							Agencia temp = lista_de_agencias.get(i);
							temp.print();
						}

					System.out.println("");
					break;
				case 2:
					System.out.println("-----Opção 2 selecionada.-----");
					System.out.println(" Cadastrar Funcionario: ");
					System.out.println("");

					Scanner f = new Scanner(System.in);
					System.out.print(" Quantos funcionarios deseja cadastrar?: ");
					int fun = f.nextInt();

					System.out.println("");

					for(int i = 0; i < fun; i++) {
						Funcionario funcionario = new Funcionario();
						funcionario.scan();
						lista_de_funcionarios.add(funcionario);
						System.out.print("");
					}

					System.out.println("");
					break;
				case 3:
					System.out.println("-----Opção 3 selecionada.-----");
					System.out.println(" Cadastrar Carros");

					for (int i = 0; i < lista_de_agencias.size(); i++){
						lista_de_agencias.get(i).print();
					}

					Scanner x = new Scanner(System.in);
					System.out.print("  Digite o código da agência: ");
					int z = x.nextInt();
					System.out.println("");

					for (int i = 0; i < lista_de_agencias.size(); i++) {
						if (lista_de_agencias.get(i).codigoAgencia == z)
							lista_de_agencias.get(i).cadastraCarro();
					}


					System.out.println("");
					break;
				case 4:
					System.out.println("-----Opção 4 selecionada.-----");
					System.out.println(" Cadastrar Cliente: ");
					System.out.print("");

					Cliente cliente = new Cliente();
					cliente.scan();
					lista_de_clientes.add(cliente);

					System.out.println("");
					break;
				case 5:
					System.out.println("-----Opção 5 selecionada.-----");
					System.out.println("  Aluguel de veículo: ");
					System.out.println("");

					Scanner cod = new Scanner(System.in);
					System.out.print("  Digite o código da agência: ");
					int codagencia = cod.nextInt();

					System.out.println("");
					System.out.println("  Carros disponíveis na agência: ");
					for (int i = 0; i < lista_de_agencias.size(); i++) {
						if(lista_de_agencias.get(i).codigoAgencia == codagencia) {

							for(int j = 0; j < lista_de_agencias.get(i).lista_de_carros.size(); j++){
								if(lista_de_agencias.get(i).lista_de_carros.get(j).alugado == false)
									lista_de_agencias.get(i).imprimeCarro();
							}
						}
					}

					Aluguel aluguel = new Aluguel();
					aluguel.scan();
					System.out.print("");

					for(int y = 0; y < lista_de_funcionarios.size(); y++){
						if(lista_de_funcionarios.get(y).senha.equals( aluguel.senhaFunc )){

							for (int l = 0; l < lista_de_clientes.size(); l++) {
								if(lista_de_clientes.get(l).cnh.equals( aluguel.cnhCliente )){

									for (int i = 0; i < lista_de_agencias.size(); i++) {
										if(lista_de_agencias.get(i).codigoAgencia == codagencia) {

											for(int j = 0; j < lista_de_agencias.get(i).lista_de_carros.size(); j++){
												if(lista_de_agencias.get(i).lista_de_carros.get(j).placa.equals( aluguel.placa )){
													lista_de_agencias.get(i).lista_de_carros.get(j).alugado = true;

													System.out.print("");
													lista_de_agencias.get(i).imprimeCarro();
													System.out.println("  Aluguel efetuado com sucesso!");
												}
											}
										}
									}
								}
							}
						}
					}

					System.out.println("");
					break;
				case 6:
					System.out.println("-----Opção 6 selecionada.-----");
					System.out.println(" Entrega de veículo: ");
					System.out.print("");

					Scanner c1 = new Scanner(System.in);
					System.out.print("  Digite o código da agência que o carro foi alugado: ");
					int cod1 = c1.nextInt();

					Scanner s = new Scanner(System.in);
					System.out.print("  Digite a placa do carro: ");
					String placa = s.nextLine();

					for (int i = 0; i < lista_de_agencias.size(); i++) {
						if(lista_de_agencias.get(i).codigoAgencia == cod1) {

							for(int j = 0; j < lista_de_agencias.get(i).lista_de_carros.size(); j++){
								if(lista_de_agencias.get(i).lista_de_carros.get(j).placa.equals( placa )){

									lista_de_agencias.get(i).lista_de_carros.remove(j);
									System.out.println("    Carro removido com sucesso!");
									System.out.print("");
								}
							}
						}
					}

					Scanner c2 = new Scanner(System.in);
					System.out.print("  Digite o código da agência que o carro vai ser entregue: ");
					int cod2 = c2.nextInt();
					System.out.println("");


					for (int i = 0; i< lista_de_agencias.size(); i++) {
						if (lista_de_agencias.get(i).codigoAgencia == cod2)
							lista_de_agencias.get(i).cadastraCarro();
							System.out.print("      Carro adicionado com sucesso!");
					}

					System.out.println("");
					break;
				default:
					System.out.println(" Programa finalizado!");
			}
		} while(opcao != 0);
	}
}

